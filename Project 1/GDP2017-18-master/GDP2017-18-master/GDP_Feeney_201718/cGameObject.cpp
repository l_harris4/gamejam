#include "cGameObject.h"
#include <vector>
#include "globalGameStuff.h"
#include <map>
#include <glm/gtc/matrix_transform.hpp>
#include <glm\gtx\intersect.hpp>
#include "cTriangle.h"

//glm::vec3 testNormal = glm::vec3(1, 0, 1);
//glm::vec3 pointOnPlane = glm::vec3(-1, 0, 0);



// Start the unique IDs at 1. Why not?
/*static*/ unsigned int cGameObject::m_nextUniqueID = 1;

cGameObject::cGameObject()
{
	this->bDiscardTexture = false;
	this->bTransparentTexture = false;
	this->scale = 1.0f;	// (not zero)
	this->position = glm::vec3(0.0f);
	//this->orientation = glm::vec3(0.0f);
	//this->orientation2 = glm::vec3(0.0f);
	this->qOrientation = glm::quat(glm::vec3(0.0f, 0.0f, 0.0f));

	this->vel = glm::vec3(0.0f);
	this->accel = glm::vec3(0.0f);

	// If you aren't sure what the 4th value should be, 
	//	make it a 1.0f ("alpha" or transparency)
	this->diffuseColour = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);

	//Assume everything is simulated 
	this->bIsUpdatedInPhysics = true; //??? 
	this->radius = 0.0f;	// Is this the best value??? Who knows?

	this->typeOfObject = eTypeOfObject::UNKNOWN;	// Technically non-standard
	//this->typeOfObject = UNKNOWN;

	this->bIsWireFrame = false;

	// Set all texture blend values to 0.0f (meaning NO texture)
	for (int index = 0; index != NUMTEXTURES; index++)
	{
		this->textureBlend[index] = 0.0f;
	}

	// Assign unque ID, the increment for next created object
	// (Note: if you write your own copy constructor, be sure to COPY this
	//	value, rather than generate a new one - i.e. call the c'tor again)
	this->m_UniqueID = cGameObject::m_nextUniqueID++;

	return;
}

cGameObject::~cGameObject()
{
	return;
}

void cGameObject::overwriteQOrientationFormEuler(glm::vec3 eulerAxisOrientation)
{
	// Calcualte the quaternion represnetaiton of this Euler axis
	// NOTE: We are OVERWRITING this..
	this->qOrientation = glm::quat(eulerAxisOrientation);


	return;
}

void cGameObject::adjustQOrientationFormDeltaEuler(glm::vec3 eulerAxisOrientChange)
{
	// How do we combine two matrices?
	// That's also how we combine quaternions...

	// So we want to "add" this change in oriention
	glm::quat qRotationChange = glm::quat(eulerAxisOrientChange);

	// Mulitply it by the current orientation;
	this->qOrientation = this->qOrientation * qRotationChange;

	return;
}

bool cGameObject::RightSideOfPlane(sVertex_xyz_rgba_n_uv2_bt&const vertex, glm::vec3&const pointOnPlane, glm::vec3&const testNormal)
{


	//will have to apply object translation, orientation, and scale
	glm::vec4 tempPos4 = glm::vec4(vertex.x, vertex.y, vertex.z, 1);
	//tempPos4 = tempPos4 * glm::toMat4(this->qOrientation);
	tempPos4 = tempPos4 * this->worldMatrix;
	//tempPos 

	glm::vec3 tempPos = glm::vec3(tempPos4.x, tempPos4.y, tempPos4.z);

	glm::vec3 direction(tempPos - pointOnPlane);

	double dprod = glm::dot(testNormal, direction);

	if (dprod <= 0)
	{
		return true;
	}
	return false;
}

void cGameObject::RecalculateWorldMatrix()
{
	//recalulate the world matrix
	this->worldMatrix = glm::mat4x4(1.0f);
	glm::mat4 trans = glm::mat4x4(1.0f);
	trans = glm::translate(trans, this->position);
	this->worldMatrix = this->worldMatrix * trans;

	glm::mat4 postRotQuat = glm::mat4(this->qOrientation);
	this->worldMatrix = this->worldMatrix * postRotQuat;

	// The scale is relative to the original model
	glm::mat4 matScale = glm::mat4x4(1.0f);
	matScale = glm::scale(matScale,
		glm::vec3(this->scale,
			this->scale,
			this->scale));
	this->worldMatrix = this->worldMatrix * matScale;
	this->inverseWorldMatrix = glm::inverse(worldMatrix);

}

void cGameObject::ReorientPositionAndVertices(std::vector<sVertex_xyz_rgba_n_uv2_bt>* vertices, std::vector<cTriangle>* triangles)
{
	glm::vec3 holderPosition = this->position;
	glm::vec3 maxXYZ = glm::vec3(-9999, -9999, -9999);
	glm::vec3 minXYZ = glm::vec3(9999, 9999, 9999);


	std::map<int, int> consideredVertices;

	//instead of going through all of the vertices, should go through all the triangles
	//as that is now a better representation of what the model currently is

	//now go through all the points applying the model matrix
	//calculate a max xyz and min xyz
	int trianglesSize = triangles->size();
	for (int i = 0; i < trianglesSize; ++i)
	{

		if (consideredVertices[(*triangles)[i].vertex_ID_0] != 1)
		{
			glm::vec4 tempPos4In = glm::vec4((*vertices)[(*triangles)[i].vertex_ID_0].x, (*vertices)[(*triangles)[i].vertex_ID_0].y, (*vertices)[(*triangles)[i].vertex_ID_0].z, 1);
			tempPos4In = tempPos4In * this->worldMatrix;
			glm::vec3 tempPosIn = glm::vec3(tempPos4In.x, tempPos4In.y, tempPos4In.z);

			if (tempPosIn.x < minXYZ.x)
				minXYZ.x = tempPosIn.x;
			if (tempPosIn.y < minXYZ.y)
				minXYZ.y = tempPosIn.y;
			if (tempPosIn.z < minXYZ.z)
				minXYZ.z = tempPosIn.z;

			if (tempPosIn.x > maxXYZ.x)
				maxXYZ.x = tempPosIn.x;
			if (tempPosIn.y > maxXYZ.y)
				maxXYZ.y = tempPosIn.y;
			if (tempPosIn.z > maxXYZ.z)
				maxXYZ.z = tempPosIn.z;

			consideredVertices[(*triangles)[i].vertex_ID_0] = 1;
		}

		if (consideredVertices[(*triangles)[i].vertex_ID_1] != 1)
		{
			glm::vec4 tempPos4In = glm::vec4((*vertices)[(*triangles)[i].vertex_ID_1].x, (*vertices)[(*triangles)[i].vertex_ID_1].y, (*vertices)[(*triangles)[i].vertex_ID_1].z, 1);
			tempPos4In = tempPos4In * this->worldMatrix;
			glm::vec3 tempPosIn = glm::vec3(tempPos4In.x, tempPos4In.y, tempPos4In.z);

			if (tempPosIn.x < minXYZ.x)
				minXYZ.x = tempPosIn.x;
			if (tempPosIn.y < minXYZ.y)
				minXYZ.y = tempPosIn.y;
			if (tempPosIn.z < minXYZ.z)
				minXYZ.z = tempPosIn.z;

			if (tempPosIn.x > maxXYZ.x)
				maxXYZ.x = tempPosIn.x;
			if (tempPosIn.y > maxXYZ.y)
				maxXYZ.y = tempPosIn.y;
			if (tempPosIn.z > maxXYZ.z)
				maxXYZ.z = tempPosIn.z;

			consideredVertices[(*triangles)[i].vertex_ID_1] = 1;
		}

		if (consideredVertices[(*triangles)[i].vertex_ID_2] != 1)
		{
			glm::vec4 tempPos4In = glm::vec4((*vertices)[(*triangles)[i].vertex_ID_2].x, (*vertices)[(*triangles)[i].vertex_ID_2].y, (*vertices)[(*triangles)[i].vertex_ID_2].z, 1);
			tempPos4In = tempPos4In * this->worldMatrix;
			glm::vec3 tempPosIn = glm::vec3(tempPos4In.x, tempPos4In.y, tempPos4In.z);

			if (tempPosIn.x < minXYZ.x)
				minXYZ.x = tempPosIn.x;
			if (tempPosIn.y < minXYZ.y)
				minXYZ.y = tempPosIn.y;
			if (tempPosIn.z < minXYZ.z)
				minXYZ.z = tempPosIn.z;

			if (tempPosIn.x > maxXYZ.x)
				maxXYZ.x = tempPosIn.x;
			if (tempPosIn.y > maxXYZ.y)
				maxXYZ.y = tempPosIn.y;
			if (tempPosIn.z > maxXYZ.z)
				maxXYZ.z = tempPosIn.z;

			consideredVertices[(*triangles)[i].vertex_ID_2] = 1;
		}

	}


	//use that to calculate an average point
	glm::vec3 newPosition = glm::vec3(0);
	newPosition.x = (maxXYZ.x + minXYZ.x) / 2.0f;
	newPosition.y = (maxXYZ.y + minXYZ.y) / 2.0f;
	newPosition.z = (maxXYZ.z + minXYZ.z) / 2.0f;

	//note this might have to be turned around
	glm::vec3 difference = newPosition - position;

	//use if the difference between the average point and the current location to
	//modify all of the vertices //apply model matrix, translate, apply reverse of model matrix (not sure if reverse of new or old)
	int verticesSize = vertices->size();
	for (int i = 0; i < verticesSize; ++i)
	{
		glm::vec4 tempPos4In = glm::vec4((*vertices)[i].x, (*vertices)[i].y, (*vertices)[i].z, 1);
		tempPos4In = tempPos4In * this->worldMatrix;
		glm::vec3 tempPosIn = glm::vec3(tempPos4In.x, tempPos4In.y, tempPos4In.z);

		tempPosIn = tempPosIn + difference;
		tempPos4In = glm::vec4(tempPosIn.x, tempPosIn.y, tempPosIn.z, 1);

		//note the inverse world matrix may have to be updated
		tempPos4In = tempPos4In * this->inverseWorldMatrix;
		(*vertices)[i].x = tempPos4In.x;
		(*vertices)[i].y = tempPos4In.y;
		(*vertices)[i].z = tempPos4In.z;

	}

	this->position = newPosition;
}

void SphereEdge(glm::vec3 & pointInSphere, glm::vec3& center, float& radius)
{
	pointInSphere = (glm::normalize(pointInSphere - center))* (radius + 0.1f) + center;
}

glm::vec3 cGameObject::FindIntersectionPoint(sVertex_xyz_rgba_n_uv2_bt&const vertexIn, sVertex_xyz_rgba_n_uv2_bt&const vertexOut, glm::vec3&const pointOnPlane, glm::vec3&const testNormal)
{
	glm::vec4 tempPos4In = glm::vec4(vertexIn.x, vertexIn.y, vertexIn.z, 1);
	tempPos4In = tempPos4In * this->worldMatrix;
	glm::vec3 tempPosIn = glm::vec3(tempPos4In.x, tempPos4In.y, tempPos4In.z);

	glm::vec4 tempPos4Out = glm::vec4(vertexOut.x, vertexOut.y, vertexOut.z, 1);
	tempPos4Out = tempPos4Out * this->worldMatrix;
	glm::vec3 tempPosOut = glm::vec3(tempPos4Out.x, tempPos4Out.y, tempPos4Out.z);

	glm::vec3 direction = glm::normalize(tempPosOut - tempPosIn);
	float dist;
	glm::intersectRayPlane(tempPosIn, direction, pointOnPlane, testNormal, dist);

	//TODO
	//might have to somehow undo the world matrix

	glm::vec3 returnVector = (tempPosIn + (direction * dist));
	glm::vec4 tempReturnVector = glm::vec4();
	tempReturnVector.x = returnVector.x;
	tempReturnVector.y = returnVector.y;
	tempReturnVector.z = returnVector.z;
	tempReturnVector.w = 1;

	tempReturnVector = tempReturnVector * this->inverseWorldMatrix;

	returnVector.x = tempReturnVector.x;
	returnVector.y = tempReturnVector.y;
	returnVector.z = tempReturnVector.z;

	return returnVector;
}

glm::vec3 cGameObject::AveragePoint(sVertex_xyz_rgba_n_uv2_bt&const vertex1, sVertex_xyz_rgba_n_uv2_bt&const vertex2)
{
	glm::vec4 tempPos4In = glm::vec4(vertex1.x, vertex1.y, vertex1.z, 1);
	tempPos4In = tempPos4In * this->worldMatrix;
	glm::vec3 tempPosIn = glm::vec3(tempPos4In.x, tempPos4In.y, tempPos4In.z);

	glm::vec4 tempPos4Out = glm::vec4(vertex2.x, vertex2.y, vertex2.z, 1);
	tempPos4Out = tempPos4Out * this->worldMatrix;
	glm::vec3 tempPosOut = glm::vec3(tempPos4Out.x, tempPos4Out.y, tempPos4Out.z);


	glm::vec3 returnVector = tempPosIn + tempPosOut;
	returnVector *= 0.5f;
	return returnVector;
}

void CopyVertexValues(sVertex_xyz_rgba_n_uv2_bt* first, sVertex_xyz_rgba_n_uv2_bt* second)
{
	first->a = second->a;
	first->b = second->b;
	first->bx = second->bx;
	first->by = second->by;
	first->bz = second->bz;
	first->g = second->g;
	first->nx = second->nx;
	first->ny = second->ny;
	first->nz = second->nz;
	first->r = second->r;
	first->tx = second->tx;
	first->ty = second->ty;
	first->tz = second->tz;
	first->u1 = second->u1;
	first->u2 = second->u2;
	first->v1 = second->v1;
	first->v2 = second->v2;
	first->x = second->x;
	first->y = second->y;
	first->z = second->z;
}

void AverageVertex(sVertex_xyz_rgba_n_uv2_bt* first, sVertex_xyz_rgba_n_uv2_bt* second, sVertex_xyz_rgba_n_uv2_bt* third)
{
	first->a = second->a;
	first->b = second->b;
	first->bx = (second->bx + third->bx) / 2.0f;
	first->by = (second->by + third->by) / 2.0f;
	first->bz = (second->bz + third->bz) / 2.0f;
	first->g = second->g;
	first->nx = (second->nx + third->nx) / 2.0f;
	first->ny = (second->ny + third->ny) / 2.0f;
	first->nz = (second->nz + third->nz) / 2.0f;
	first->r = second->r;
	first->tx = second->tx;
	first->ty = second->ty;
	first->tz = second->tz;
	first->u1 = (second->u1 + third->u1) / 2.0f;
	first->u2 = (second->u2 + third->u2) / 2.0f;
	first->v1 = (second->v1 + third->v1) / 2.0f;
	first->v2 = (second->v2 + third->v2) / 2.0f;
	first->x = (second->x + third->x) / 2.0f;
	first->y = (second->y + third->y) / 2.0f;
	first->z = (second->z + third->z) / 2.0f;
}

void eraseMeshPartP(bool makeNewMesh, glm::vec3 pointOnPlane, glm::vec3 testNormal, cGameObject* theObject)
{
	theObject->RecalculateWorldMatrix();

	cMesh* theOldMesh = g_pVAOManager->lookupMeshFromName(theObject->vecMeshes[0].name);
	cMesh theNewMesh;
	//basically make a new mesh using the old mesh
	theNewMesh.maxExtent = theOldMesh->maxExtent;
	theNewMesh.maxExtentXYZ = theOldMesh->maxExtentXYZ;
	theNewMesh.maxXYZ = theOldMesh->maxXYZ;
	theNewMesh.minXYZ = theOldMesh->minXYZ;
	theNewMesh.name = theOldMesh->name;
	theNewMesh.scaleForUnitBBox = theOldMesh->scaleForUnitBBox;

	theObject->vecMeshes[0].bDisableBackFaceCulling = true;

	//being deleted when it goes out of scope
	//maybe change triangles and vertices into a pointer, then delete old ones before loading these into the mesh
	std::vector<sVertex_xyz_rgba_n_uv2_bt>* vertices = new std::vector<sVertex_xyz_rgba_n_uv2_bt>();
	bool meshInCut = false;

	for (int i = 0; i < theOldMesh->numberOfVertices; ++i)
	{
		vertices->push_back(theOldMesh->pVertices[i]);
	}



	std::vector<cTriangle>* triangles = new std::vector<cTriangle>();
	std::vector<std::pair<int, int>> pairs;
	//std::map<std::pair<int, int>, glm::vec3> intersectionPoints;
	int outSideCount = 0;
	bool firstInside = false;
	bool secondInside = false;
	bool thirdInside = false;

	for (int i = 0; i < theOldMesh->numberOfTriangles; ++i)
	{
		outSideCount = 0;
		firstInside = true;
		secondInside = true;
		thirdInside = true;

		if (theObject->RightSideOfPlane(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], pointOnPlane, testNormal))
		{
			outSideCount++;
			firstInside = false;
		}
		if (theObject->RightSideOfPlane(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1], pointOnPlane, testNormal))
		{
			outSideCount++;
			secondInside = false;
		}
		if (theObject->RightSideOfPlane(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2], pointOnPlane, testNormal))
		{
			outSideCount++;
			thirdInside = false;
		}

		if (outSideCount == 0)
			triangles->push_back(theOldMesh->pTriangles[i]);
		else if (outSideCount == 1)
		{
			meshInCut = true;

			sVertex_xyz_rgba_n_uv2_bt* firstVert = new sVertex_xyz_rgba_n_uv2_bt();
			sVertex_xyz_rgba_n_uv2_bt* secondVert = new sVertex_xyz_rgba_n_uv2_bt();
			sVertex_xyz_rgba_n_uv2_bt* thirdVert = new sVertex_xyz_rgba_n_uv2_bt();

			glm::vec3 intersection1 = glm::vec3(0);
			glm::vec3 intersection2 = glm::vec3(0);

			glm::vec3 middlePoint = glm::vec3(0);


			if (!firstInside)
			{

				intersection1 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], pointOnPlane, testNormal);

				intersection2 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], pointOnPlane, testNormal);

				middlePoint = theObject->AveragePoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2]);
				CopyVertexValues(firstVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0]);
				CopyVertexValues(secondVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0]);
				AverageVertex(thirdVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1], &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2]);
			}
			else if (!secondInside)
			{

				intersection1 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1], pointOnPlane, testNormal);

				intersection2 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1], pointOnPlane, testNormal);


				middlePoint = theObject->AveragePoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2]);
				CopyVertexValues(firstVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1]);
				CopyVertexValues(secondVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1]);
				AverageVertex(thirdVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2]);
			}
			else
			{

				intersection1 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2], pointOnPlane, testNormal);

				intersection2 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2], pointOnPlane, testNormal);

				middlePoint = theObject->AveragePoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1]);
				CopyVertexValues(firstVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2]);
				CopyVertexValues(secondVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2]);
				AverageVertex(thirdVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1]);
			}

			firstVert->x = intersection1.x;
			firstVert->y = intersection1.y;
			firstVert->z = intersection1.z;

			secondVert->x = intersection2.x;
			secondVert->y = intersection2.y;
			secondVert->z = intersection2.z;

			//and to the vertices vector
			vertices->push_back(*firstVert);
			vertices->push_back(*secondVert);

			int size = vertices->size();
			//add those points to the pairs
			pairs.push_back(std::make_pair<int, int>(size - 1, size - 2));

			vertices->push_back(*thirdVert);
			size = vertices->size();

			//create and add the three new triangles
			cTriangle* firstTri = new cTriangle();
			cTriangle* secondTri = new cTriangle();
			cTriangle* thirdTri = new cTriangle();

			if (!firstInside)
			{
				firstTri->vertex_ID_0 = theOldMesh->pTriangles[i].vertex_ID_1;
				secondTri->vertex_ID_0 = theOldMesh->pTriangles[i].vertex_ID_2;
			}
			else if (!secondInside)
			{
				firstTri->vertex_ID_0 = theOldMesh->pTriangles[i].vertex_ID_0;
				secondTri->vertex_ID_0 = theOldMesh->pTriangles[i].vertex_ID_2;
			}
			else
			{
				firstTri->vertex_ID_0 = theOldMesh->pTriangles[i].vertex_ID_0;
				secondTri->vertex_ID_0 = theOldMesh->pTriangles[i].vertex_ID_1;
			}


			firstTri->vertex_ID_1 = size - 3;
			firstTri->vertex_ID_2 = size - 1;

			secondTri->vertex_ID_1 = size - 2;
			secondTri->vertex_ID_2 = size - 1;


			thirdTri->vertex_ID_0 = size - 1;
			thirdTri->vertex_ID_1 = size - 2;
			thirdTri->vertex_ID_2 = size - 3;



			triangles->push_back(*firstTri);
			triangles->push_back(*secondTri);
			triangles->push_back(*thirdTri);


		}
		else if (outSideCount == 2)
		{
			meshInCut = true;
			//use the vertex inside Id,
			glm::vec3 intersection1 = glm::vec3(0);
			glm::vec3 intersection2 = glm::vec3(0);
			sVertex_xyz_rgba_n_uv2_bt* firstVert = new sVertex_xyz_rgba_n_uv2_bt();
			sVertex_xyz_rgba_n_uv2_bt* secondVert = new sVertex_xyz_rgba_n_uv2_bt();

			//the triangle we will be constructing
			cTriangle* newTri = new cTriangle();

			bool alreadyExistingIntersection = false;


			//find the two intersection points
			if (firstInside)
			{

				intersection1 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1], pointOnPlane, testNormal);

				intersection2 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2], pointOnPlane, testNormal);

				CopyVertexValues(firstVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1]);
				CopyVertexValues(secondVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2]);
				newTri->vertex_ID_0 = theOldMesh->pTriangles[i].vertex_ID_0;
			}
			else if (secondInside)
			{

				intersection1 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], pointOnPlane, testNormal);

				intersection2 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2], pointOnPlane, testNormal);

				CopyVertexValues(firstVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0]);
				CopyVertexValues(secondVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2]);
				newTri->vertex_ID_0 = theOldMesh->pTriangles[i].vertex_ID_1;
			}
			else
			{

				intersection1 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1], pointOnPlane, testNormal);

				intersection2 = theObject->FindIntersectionPoint(theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_2], theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0], pointOnPlane, testNormal);

				CopyVertexValues(firstVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_0]);
				CopyVertexValues(secondVert, &theOldMesh->pVertices[theOldMesh->pTriangles[i].vertex_ID_1]);
				newTri->vertex_ID_0 = theOldMesh->pTriangles[i].vertex_ID_2;
			}

			//set firstVert to intersection one location
			firstVert->x = intersection1.x;
			firstVert->y = intersection1.y;
			firstVert->z = intersection1.z;

			secondVert->x = intersection2.x;
			secondVert->y = intersection2.y;
			secondVert->z = intersection2.z;

			//and to the vertices vector
			vertices->push_back(*firstVert);
			vertices->push_back(*secondVert);
			int size = vertices->size();
			//add those points to the pairs
			pairs.push_back(std::make_pair<int, int>(size - 1, size - 2));

			//make a new triangle using the vertex inside and the two insersection points
			newTri->vertex_ID_1 = pairs.back().first;
			newTri->vertex_ID_2 = pairs.back().second;

			triangles->push_back(*newTri);

		}
	}

	if (meshInCut)
	{
		//make a final vertex
		sVertex_xyz_rgba_n_uv2_bt* finalVert = new sVertex_xyz_rgba_n_uv2_bt();
		CopyVertexValues(finalVert, &(*vertices)[pairs.front().first]);
		//average it based on all the pairs 
		int pairsSize = pairs.size();
		int vertSize = vertices->size();
		glm::vec3 averagePos = glm::vec3(0);
		float divisionValue = pairsSize * 2;
		for (int i = 0; i < pairsSize; ++i)
		{
			cTriangle* newTri = new cTriangle();
			newTri->vertex_ID_0 = pairs[i].first;
			newTri->vertex_ID_1 = pairs[i].second;
			newTri->vertex_ID_2 = vertSize;

			averagePos.x += (*vertices)[pairs[i].first].x;
			averagePos.y += (*vertices)[pairs[i].first].y;
			averagePos.z += (*vertices)[pairs[i].first].z;

			averagePos.x += (*vertices)[pairs[i].second].x;
			averagePos.y += (*vertices)[pairs[i].second].y;
			averagePos.z += (*vertices)[pairs[i].second].z;

			triangles->push_back(*newTri);
		}

		averagePos = averagePos / divisionValue;
		finalVert->x = averagePos.x;
		finalVert->y = averagePos.y;
		finalVert->z = averagePos.z;

		vertices->push_back(*finalVert);

		//before reassigning the vertices make a copy of the object and call this method on it, but reverse the normal
		if (makeNewMesh)
		{
			cGameObject* theOtherHalf = new cGameObject();
			theOtherHalf->vecMeshes = theObject->vecMeshes;

			theOtherHalf->bIsUpdatedInPhysics = theObject->bIsUpdatedInPhysics;
			theOtherHalf->bIsWireFrame = theObject->bIsWireFrame;
			theOtherHalf->position = theObject->position;
			theOtherHalf->scale = theObject->scale;
			theOtherHalf->meshName = theObject->meshName + "2";
			theOtherHalf->worldMatrix = theObject->worldMatrix;
			theOtherHalf->qOrientation = theObject->qOrientation;
			::g_vecGameObjects.push_back(theOtherHalf);

			::g_vecGameObjects.back()->eraseMeshPart(false, pointOnPlane, testNormal * -1.0f);
			::g_vecGameObjects.back()->position += testNormal * 0.5f;
			::g_vecGameObjects.back()->vecMeshes[0].bDisableBackFaceCulling = true;
			::g_vecGameObjects.back()->cuttable = true;
		}
		//dunno why, but these break on the second run through, the deletes that is
		//delete[] theOldMesh->pVertices;


		if (makeNewMesh)
		{
			//there is probably going to be a problem with them sharing the same mesh
			theNewMesh.name = theOldMesh->name + "1";
			theObject->meshName = theObject->meshName + "1";
			theObject->vecMeshes[0].name = theObject->vecMeshes[0].name + "1";

			theObject->ReorientPositionAndVertices(vertices, triangles);
		}
		else
		{
			theNewMesh.name = theOldMesh->name + "2";
			theObject->vecMeshes[0].name = theObject->vecMeshes[0].name + "2";
			theObject->ReorientPositionAndVertices(vertices, triangles);
		}

		theNewMesh.pVertices = &(*vertices)[0];
		theNewMesh.numberOfVertices = vertices->size();

		theNewMesh.numberOfTriangles = triangles->size();
		//delete[] theOldMesh->pTriangles;
		theNewMesh.pTriangles = &(*triangles)[0];

		GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");
		g_pVAOManager->loadMeshIntoVAO(theNewMesh, sexyShaderID);

	}
	//experiment to try and fix a broken thing
	//theOldMesh = theNewMesh;


}

void cGameObject::eraseMeshPart(bool makeNewMesh, glm::vec3 pointOnPlane, glm::vec3 testNormal)
{

	eraseMeshPartP(makeNewMesh, pointOnPlane, testNormal, this);

}

void cGameObject::DeleteChildren(void)
{
	for (std::vector< cGameObject* >::iterator itChild = this->vec_pChildObjects.begin();
		itChild != this->vec_pChildObjects.end(); itChild++)
	{
		// Pointer not zero (0)?
		cGameObject* pTempChildObject = (*itChild);
		if (pTempChildObject != 0)
		{
			// Recursively delete all children's children (and so on)
			pTempChildObject->DeleteChildren();
			// Now delete this child
			delete pTempChildObject;
		}
	}
	// There's a vector, but nothing in it
	this->vec_pChildObjects.clear();
	return;
}

cGameObject* cGameObject::FindChildByFriendlyName(std::string name)
{
	for (std::vector<cGameObject*>::iterator itCGO = this->vec_pChildObjects.begin(); itCGO != this->vec_pChildObjects.end(); itCGO++)
	{
		if ((*itCGO)->friendlyName == name)
		{
			return (*itCGO);
		}
	}
	// Didn't find it.
	return NULL;
}

cGameObject* cGameObject::FindChildByID(unsigned int ID)
{
	for (std::vector<cGameObject*>::iterator itCGO = this->vec_pChildObjects.begin(); itCGO != this->vec_pChildObjects.end(); itCGO++)
	{
		if ((*itCGO)->getUniqueID() == ID)
		{
			return (*itCGO);
		}
	}
	// Didn't find it.
	return NULL;
}


//glm::quat cGameObject::getFinalMeshQOrientation(void)
//{
//	return this->m_PhysicalProps.qOrientation * this->m_meshQOrientation;
//}

glm::quat cGameObject::getFinalMeshQOrientation(unsigned int meshID)
{	// Does NOT check for the index of the mesh!
	return this->qOrientation * this->vecMeshes[meshID].getQOrientation();
}

glm::quat cGameObject::getFinalMeshQOrientation(glm::quat &meshQOrientation)
{	// Does NOT check for the index of the mesh!
	return this->qOrientation * meshQOrientation;
}


