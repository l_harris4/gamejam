#include "globalOpenGL_GLFW.h"
#include "globalGameStuff.h"

#include <iostream>

//glfwGetKey

//inline bool 

bool isShiftKeyDown(int mods, bool bByItself = true);
bool isCtrlKeyDown(int mods, bool bByItself = true);
bool isAltKeyDown(int mods, bool bByItself = true);

extern int g_selectedGameObjectIndex;
extern int g_selectedLightIndex;
extern bool g_movingGameObject;
extern bool g_lightsOn;
extern bool g_texturesOn;
extern bool g_movingLights;
extern bool g_boundingBoxes;
extern cCamera* g_pTheCamera;

const float MOVESPEED = 0.3f;
const float CAMERAROTATIONSPEED = 1;
const float CAMERASPEED = 2;

glm::vec3 spherePos = glm::vec3(2, 2, 1.2);
float sphereSize = 1;

extern bool goingDown;
//extern cGameObject* cameraTargetObject;

void updateCamera(glm::vec3 turnChange)
{
	g_pTheCamera->adjustQOrientationFormDeltaEuler(turnChange);

	glm::vec4 tempPos = glm::toMat4(g_pTheCamera->qCameraOrientation) *
		glm::vec4(0, 10, g_pTheCamera->zDist, 0);


	glm::vec3 objectPos;
	objectPos = g_pTheCamera->theObject->position;
	glm::vec3 position = glm::vec3(tempPos.x + objectPos.x
		, tempPos.y + objectPos.y,
		tempPos.z + objectPos.z);

	g_pTheCamera->eye = position;
}




/*static*/ void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);

	cGameObject* pLeftTeapot = findObjectByFriendlyName(LEFTTEAPOTNAME, ::g_vecGameObjects);

	switch (key)
	{
	case GLFW_KEY_SPACE:
	{
		if (action == GLFW_PRESS)
		{
			goingDown = true;
			//glm::vec3 testNormal = glm::vec3(1, 1, 1);
			//glm::vec3 pointOnPlane = glm::vec3(-1, 0, 0);
			//::g_vecGameObjects[0]->eraseMeshPart(true, pointOnPlane, testNormal);
		}
	}
	break;

	case GLFW_KEY_T:
	{
		if (action == GLFW_PRESS)
		{
			//::g_vecGameObjects[0]->vecMeshes[0].bDrawAsWireFrame = !::g_vecGameObjects[0]->vecMeshes[0].bDrawAsWireFrame;
			::g_vecGameObjects[0]->vecMeshes[0].bDisableBackFaceCulling = !::g_vecGameObjects[0]->vecMeshes[0].bDisableBackFaceCulling;
			//::g_vecGameObjects[4]->vecMeshes[0].bDisableBackFaceCulling = !::g_vecGameObjects[4]->vecMeshes[0].bDisableBackFaceCulling;
		}
	}
	break;

	case GLFW_KEY_U:
	{
		if (action == GLFW_PRESS)
		{
			goingDown = true;
		}
	}
	break;
	case GLFW_KEY_O:
	{
		if (action == GLFW_PRESS)
		{
			//spherePos.x += 0.1;
			//::g_vecGameObjects[0]->eraseMeshPart(spherePos, sphereSize, false);
		}
	}
	break;
	case GLFW_KEY_I:
	{
		if (action == GLFW_PRESS)
		{
			spherePos.y += 0.1;
		}
	}
	break;
	case GLFW_KEY_K:
	{
		if (action == GLFW_PRESS)
		{
			spherePos.y -= 0.1;
		}
	}
	break;
	case GLFW_KEY_J:
	{
		if (action == GLFW_PRESS)
		{
			spherePos.z += 0.1;
		}
	}
	break;
	case GLFW_KEY_L:
	{
		if (action == GLFW_PRESS)
		{
			spherePos.z -= 0.1;
		}
	}
	break;
	case GLFW_KEY_A:
	{
		updateCamera(glm::vec3(0, glm::radians(CAMERAROTATIONSPEED), 0));
	}
	break;
	case GLFW_KEY_D:
	{
		updateCamera(glm::vec3(0, glm::radians(-CAMERAROTATIONSPEED), 0));
	}
	break;
	case GLFW_KEY_W:
	{
		g_pTheCamera->zDist += 0.5;
		g_pTheCamera->updateTick(0);
		//updateCamera(glm::vec3(glm::radians(CAMERAROTATIONSPEED), 0, 0));
	}
	break;
	case GLFW_KEY_S:
	{
		g_pTheCamera->zDist -= 0.5;
		g_pTheCamera->updateTick(0);
		//updateCamera(glm::vec3(glm::radians(-CAMERAROTATIONSPEED), 0, 0));
	}
	break;


	}
	// HACK: print output to the console
//	std::cout << "Light[0] linear atten: "
//		<< ::g_pLightManager->vecLights[0].attenuation.y << ", "
//		<< ::g_pLightManager->vecLights[0].attenuation.z << std::endl;
	return;
}



// Helper functions
bool isShiftKeyDown(int mods, bool bByItself /*=true*/)
{
	if (bByItself)
	{	// shift by itself?
		if (mods == GLFW_MOD_SHIFT) { return true; }
		else { return false; }
	}
	else
	{	// shift with anything else, too
		if ((mods && GLFW_MOD_SHIFT) == GLFW_MOD_SHIFT) { return true; }
		else { return false; }
	}
	// Shouldn't never get here, so return false? I guess?
	return false;
}

//bool isCtrlKeyDown( int mods, bool bByItself = true );
//bool isAltKeyDown( int mods, bool bByItself = true );