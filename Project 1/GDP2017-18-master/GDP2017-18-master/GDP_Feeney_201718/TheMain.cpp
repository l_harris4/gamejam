// Include glad and GLFW in correct order
#include "globalOpenGL_GLFW.h"


#include <iostream>			// C++ cin, cout, etc.
//#include "linmath.h"
#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr


#include <stdlib.h>
#include <stdio.h>
// Add the file stuff library (file stream>
#include <fstream>
#include <sstream>		// "String stream"
#include <string>
#include <time.h>

#include <vector>		//  smart array, "array" in most languages
#include "Utilities.h"
#include "ModelUtilities.h"
#include "cMesh.h"
#include "cShaderManager.h" 
#include "cGameObject.h"
#include "cVAOMeshManager.h"
#include "cModelAssetLoader.h"
#include <algorithm>
#include "cCamera.h"
#include <list>
#include <algorithm>


#include "Physics.h"	// Physics collision detection functions

#include "cLightManager.h"

// Include all the things that are accessed in other files
#include "globalGameStuff.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>

//void DrawParticle(cParticle* pThePart);
void setCubeSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID);

extern void updateCamera();



// Used by the light drawing thingy
// Will draw a wireframe sphere at this location with this colour
//void DrawDebugSphere(glm::vec3 location, glm::vec4 colour, float scale);
//cGameObject* g_pTheDebugSphere;

//	static const int MAXNUMBEROFGAMEOBJECTS = 10;
//	cGameObject* g_GameObjects[MAXNUMBEROFGAMEOBJECTS];

// Remember to #include <vector>...
std::vector< cGameObject* >  g_vecGameObjects;


cVAOMeshManager* g_pVAOManager = 0;		// or NULL, or nullptr

//cShaderManager	g_ShaderManager;			// Stack (no new)
cShaderManager*		g_pShaderManager = 0;		// Heap, new (and delete)
cLightManager*		g_pLightManager = 0;
CTextureManager*		g_pTextureManager = 0;

cDebugRenderer*			g_pDebugRenderer = 0;
cCamera* g_pTheCamera = NULL;


// Other uniforms:
GLint uniLoc_materialDiffuse = -1;
GLint uniLoc_materialAmbient = -1;
GLint uniLoc_ambientToDiffuseRatio = -1; 	// Maybe	// 0.2 or 0.3
GLint uniLoc_materialSpecular = -1;  // rgb = colour of HIGHLIGHT only
							// w = shininess of the 
GLint uniLoc_bIsDebugWireFrameObject = -1;
GLint uniLoc_bUsingLighting = -1;
GLint uniLoc_bUsingTextures = -1;
GLint uniLoc_bDiscardTexture = -1;

GLint uniLoc_eyePosition = -1;	// Camera position
GLint uniLoc_mModel = -1;
GLint uniLoc_mView = -1;
GLint uniLoc_mProjection = -1;

GLint texSampCube00_LocID = -1;
GLint texSampCube01_LocID = -1;
GLint texSampCube02_LocID = -1;
GLint texSampCube03_LocID = -1;

GLint texCubeBlend00_LocID = -1;
GLint texCubeBlend01_LocID = -1;
GLint texCubeBlend02_LocID = -1;
GLint texCubeBlend03_LocID = -1;

int g_selectedGameObjectIndex = 0;
int g_selectedLightIndex = 0;
bool g_movingGameObject = false;
bool g_lightsOn = false;
bool g_texturesOn = false;
bool g_movingLights = false;
bool g_boundingBoxes = false;
const float MOVESPEED = 0.1f;
const float ROTATIONSPEED = -2;
const float CAMERASPEED = 0.2f;

bool goingDown = false;
bool goingUp = false;
bool goingLeft = true;
bool goingRight = false;


//cGameObject* cameraTargetObject = new cGameObject();


static void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

struct DistanceFunc
{
	DistanceFunc(const glm::vec3& _p) : p(_p) {}

	bool operator()(const cGameObject* lhs, const cGameObject* rhs) const
	{
		return glm::distance(p, lhs->position) > glm::distance(p, rhs->position);
	}

private:
	glm::vec3 p;
};



int main(void)
{

	srand(time(NULL));
	GLFWwindow* window;
	//    GLuint vertex_buffer, vertex_shader, fragment_shader, program;
	GLint mvp_location;	// , vpos_location, vcol_location;
	glfwSetErrorCallback(error_callback);

	if (!glfwInit())
	{
		// exit(EXIT_FAILURE);
		std::cout << "ERROR: Couldn't init GLFW, so we're pretty much stuck; do you have OpenGL??" << std::endl;
		return -1;
	}

	int height = 700;	/* default */
	int width = 1200;	// default
	std::string title = "GAME JAM EXPERIMENT";


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	// C++ string
	// C no strings. Sorry. char    char name[7] = "Michael\0";
	window = glfwCreateWindow(width, height,
		title.c_str(),
		NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwSetKeyCallback(window, key_callback);
	glfwMakeContextCurrent(window);
	gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
	glfwSwapInterval(1);

	std::cout << glGetString(GL_VENDOR) << " "
		<< glGetString(GL_RENDERER) << ", "
		<< glGetString(GL_VERSION) << std::endl;
	std::cout << "Shader language version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

	// General error string, used for a number of items during start up
	std::string error;

	::g_pShaderManager = new cShaderManager();

	cShaderManager::cShader vertShader;
	cShaderManager::cShader fragShader;

	vertShader.fileName = "simpleVert.glsl";
	fragShader.fileName = "simpleFrag.glsl";

	::g_pShaderManager->setBasePath("assets//shaders//");

	// Shader objects are passed by reference so that
	//	we can look at the results if we wanted to. 
	if (!::g_pShaderManager->createProgramFromFile(
		"mySexyShader", vertShader, fragShader))
	{
		std::cout << "Oh no! All is lost!!! Blame Loki!!!" << std::endl;
		std::cout << ::g_pShaderManager->getLastError() << std::endl;
		// Should we exit?? 
		return -1;
		//		exit(
	}
	std::cout << "The shaders comipled and linked OK" << std::endl;


	::g_pDebugRenderer = new cDebugRenderer();
	if (!::g_pDebugRenderer->initialize(error))
	{
		std::cout << "Warning: couldn't init the debug renderer." << std::endl;
	}

	// Load models
	::g_pModelAssetLoader = new cModelAssetLoader();
	::g_pModelAssetLoader->setBasePath("assets/models/");


	::g_pVAOManager = new cVAOMeshManager();

	GLint sexyShaderID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");

	if (!Load3DModelsIntoMeshManager(sexyShaderID, ::g_pVAOManager, ::g_pModelAssetLoader, error))
	{
		std::cout << "Not all models were loaded..." << std::endl;
		std::cout << error << std::endl;
	}
	//LoadModelsIntoScene();
	::g_pLightManager = new cLightManager();
	::g_pTheCamera = new cCamera();
	::g_pTheCamera->eye = glm::vec3(-10.0f, 20.0f, 10.0f);
	::g_pTheCamera->cameraMode = ::cCamera::eMode::FOLLOW_CAMERA;
	::g_pTheCamera->target = glm::vec3(0, 0, 0);
	::g_pTheCamera->theObject = new cGameObject();


	GLint currentProgID = ::g_pShaderManager->getIDFromFriendlyName("mySexyShader");
	::g_pTextureManager = new CTextureManager();


	::g_pTextureManager->setBasePath("assets/textures");


	LoadModelsLightsFromFile();

	::g_pTextureManager->setBasePath("assets/textures/skybox");
	if (!::g_pTextureManager->CreateCubeTextureFromBMPFiles(
		"space",
		"SpaceBox_right1_posX.bmp",
		"SpaceBox_left2_negX.bmp",
		"SpaceBox_top3_posY.bmp",
		"SpaceBox_bottom4_negY.bmp",
		"SpaceBox_front5_posZ.bmp",
		"SpaceBox_back6_negZ.bmp", true, true))
	{
		std::cout << "Didn't load skybox" << std::endl;
	}



	::g_pLightManager->LoadShaderUniformLocations(currentProgID);




	glEnable(GL_DEPTH);

	// Gets the "current" time "tick" or "step"
	double lastTimeStep = glfwGetTime();
	::g_vecGameObjects[0]->cuttable = true;



	// Main game or application loop
	while (!glfwWindowShouldClose(window))
	{
		//sortObjectsBasedOnCamera();
		// Essentially the "frame time"
		// Now many seconds that have elapsed since we last checked
		double curTime = glfwGetTime();
		double deltaTime = curTime - lastTimeStep;

		::g_pTheCamera->updateTick(deltaTime);

		float moveSpeed = 0.1;

		if (!goingDown && !goingUp)
		{
			if (goingLeft)
			{
				::g_vecGameObjects[1]->position.z += moveSpeed;
				if (::g_vecGameObjects[1]->position.z >= 10)
				{
					goingLeft = false;
					goingRight = true;
				}
			}
			else if (goingRight)
			{
				::g_vecGameObjects[1]->position.z -= moveSpeed;
				if (::g_vecGameObjects[1]->position.z <= -10)
				{
					goingRight = false;
					goingLeft = true;
				}
			}
			
		}
		else if (goingDown)
		{
			::g_vecGameObjects[1]->position.y -= moveSpeed * 3;
			if (::g_vecGameObjects[1]->position.y <= 0)
			{
				goingDown = false;
				goingUp = true;
				//also do the cut
				glm::vec3 testNormal = glm::vec3(0, 0, -1);
				glm::vec3 testPos = glm::vec3(1, 0, -3);
				testPos.z = -::g_vecGameObjects[1]->position.z;
				//::g_vecGameObjects[0]->eraseMeshPart(true, ::g_vecGameObjects[1]->position, testNormal);
				//::g_vecGameObjects[0]->eraseMeshPart(true, testPos, testNormal);

				int sizeOfVecAtCut = ::g_vecGameObjects.size();
				for (int i = 0; i < sizeOfVecAtCut; ++i)
				{
					if (::g_vecGameObjects[i]->cuttable)
					{
						glm::vec3 testPosTemp = testPos;
						testPosTemp.z += ::g_vecGameObjects[i]->position.z;
						glm::vec3 testNormal = glm::vec3(0, 0, -1);
						::g_vecGameObjects[i]->eraseMeshPart(true, testPosTemp, testNormal);
					}
				}
			}
		}
		else if (goingUp)
		{
			::g_vecGameObjects[1]->position.y += moveSpeed * 3;
			if (::g_vecGameObjects[1]->position.y >= 5)
			{
				goingUp = false;
			}
		}

		RenderScene(::g_vecGameObjects, window, deltaTime);

		// "Presents" what we've drawn
		// Done once per scene 
		glfwSwapBuffers(window);
		glfwPollEvents();

	}

		glfwDestroyWindow(window);
		glfwTerminate();

		// 
		delete ::g_pShaderManager;
		delete ::g_pVAOManager;
	
	//    exit(EXIT_SUCCESS);
	return 0;
}

void setCubeSamplerAndBlenderByIndex(GLint samplerIndex, float blendRatio, GLint textureUnitID)
{
	switch (samplerIndex)
	{
	case 0:
		glUniform1i(texSampCube00_LocID, textureUnitID);
		glUniform1f(texCubeBlend00_LocID, blendRatio);
		break;
	case 1:
		glUniform1i(texSampCube01_LocID, textureUnitID);
		glUniform1f(texCubeBlend01_LocID, blendRatio);
		break;
	case 2:
		glUniform1i(texSampCube02_LocID, textureUnitID);
		glUniform1f(texCubeBlend02_LocID, blendRatio);
		break;
	case 3:
		glUniform1i(texSampCube03_LocID, textureUnitID);
		glUniform1f(texCubeBlend03_LocID, blendRatio);
		break;
	default:
		// Invalid samplerIndex;
		break;
	}//switch (samplerIndex)
	return;
}//void setCubeSamplerAndBlenderByIndex()

